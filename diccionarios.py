#Diccionario de mi mascota:

miMascota= {
    "nombre":"simon",
    "nacimiento": "abril del 2020",
    "edad": "15 meses",
    "croquetas": "Dog chow",
    "esterilizado":True,
    "vacunas": "completas",
    "muerte": "julio del 2021"
    }
        
m = miMascota.items()
print(m)

#Diccionario con datos de jugadores de futbol:

equipo= {
    "nombre" : {"Manchester City"},
    "jugador1": {"nombre": "jose molina", "edad": 34, "posicion": "Media punta", "lugar_de_nacimiento": "rosario, argentina", "fecha_de_nacimiento": "24 de junio del 1987"},  
    "jugador2": {"nombre": "carlos ramirez", "edad": 25, "posicion": "Delatera", "lugar_de_nacimiento": "funchal, portugal", "fecha_de_nacimiento": "10 de abril de 1996"},
    "jugador3": {"nombre": "daniela montoya", "edad": 31, "posicion": "defensa derecha", "lugar_de_nacimiento": "medellin, colombia", "fecha_de_nacimiento": "22 de agosto del 1990"},
    "jugador4": {"nombre": "Tobin Heath", "edad": 33, "posicion": "defensa izquierda", "lugar_de_nacimiento": "Morristown, Nueva Jersey, Estados Unidos", "fecha_de_nacimiento": "29 de mayo del 1988"},
    "jugador5": {"nombre": "hope solo", "edad": 40, "posicion": "arquera", "lugar_de_nacimiento": "Richland, Washington, Estados Unidos", "fecha_de_nacimiento": "30 de julio del 1981"},
    "partidos": {"jugados": 20, "ganados": 13, "perdidos": 7},
    "puntos": {39}
}

e = equipo.items()
print(e)